TODO: temp documentation, to translate in English eventually

### Creazione texture diffusa
Premessa: le texture sono sempre 2048x2048
1. creare gruppi di vertici seguendo la nomenclatura Col1, 2 ecc. Questo faciliterà la selezione
2. Creare texture b/n aiutandosi con gruppi di vertici, seguendo medesima nomenclatura. Bianco = area da colorare, nero = area da ignorare.
3. Controllare che non siano state selezionate facce extra perché adiacenti, ritoccandole
3. Usare nodi "MixRGB" per unire le texture dandoci colore, dove ogni nodo prende:
    * come `fac` la texture
    * come `Color1` il mix precedente (o il colore nero se è il primo)
    * come `Color2` il colore da assegnare a quella texture
4. Seguire poi l'ordine: Colori > decorazioni (ripresa colori base) > occlusione ambientale > punti luce > effetti vari (rimanendo su b/n)
5. Cuocere il risultato in una texture a parte denominata `<nome_oggetto>_diffuse`, assicurandosi che `Principled BSDF` non abbia valori alterati né altre texture connesse se non quelle che servono per la cottura

### Esportazione
1. Solo la diffusa, metallica, ruvidezza, ambientale, normale ed emissiva sono supportate dal .gltf. Le altre texture verranno ignorate.
   Seguire questi procedimenti per capire come esportarle: https://docs.blender.org/manual/en/2.80/addons/io_scene_gltf2.html#exported-materials
2. Assicurarsi che l'oggetto sia centrato alle coordinate 0,0,0
3. Selezionar ".gltf + .bin e texture" durante l'esportazione
4. Importare il tutto su Godot
